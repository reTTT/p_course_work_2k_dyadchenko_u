// p_2k_z10.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <string>

#include "quick_sort.h"

// ��������� ��� �����.
// �������� ������ � ������
// � ����������� ������� ��� ���������� �� �������� (��������� ������)
struct Chicken
{
	float weight;
	int monthsAge;
	std::string breed;
	int numberEggs;
	int id;

	// ���������� ��� ��� ���� �� � ���� ������� ��� ����������
	// �� ���� ��������� ��� ��������� ��������� ������ ���� ����� �������� ������ �������
	// ���������� ����� ������������ � ������� �����������
	// ��� ���������� � ������� �������� ����� ������� ���� � '<' �� '>'
	static bool sortByWeight(const Chicken& l, const Chicken& r)
	{
		return l.weight < r.weight;
	}

	static bool sortByYears(const Chicken& l, const Chicken& r)
	{
		return l.monthsAge < r.monthsAge;
	}

	static bool sortByBreeds(const Chicken& l, const Chicken& r)
	{
		return l.breed < r.breed;
	}

	static bool sortByNumberEggs(const Chicken& l, const Chicken& r)
	{
		return l.numberEggs < r.numberEggs;
	}

	static bool sortByID(const Chicken& l, const Chicken& r)
	{
		return l.id < r.id;
	}

	// ������� � ������� ��� ���������� � ������ ������
	static void print(const Chicken& c)
	{
		std::cout << "Chicken info ID: " << c.id << std::endl;
		std::cout << "Age(months): " << c.monthsAge << std::endl;
		std::cout << "Weight(kg): " << c.weight << std::endl;
		std::cout << "Breed: " << c.breed << std::endl;
		std::cout << "Number of eggs(months): " << c.numberEggs << std::endl;
		std::cout << std::endl;
	}

	// ������� ������ ��������� �������
	static Chicken create()
	{
		static int id = 0;
		static std::string breed[] = {"Australorp", "Barnevelder", "Hamburgh", "Dorking", "California Gray"};
		Chicken c;

		c.id = ++id;
		c.monthsAge = 3 + rand() % 20;
		c.numberEggs = 10 + rand() % 10;
		c.weight = 2.5f + (float)(rand() % 200) / 100.0f;
		c.breed = breed[rand() % 5];

		return c;
	}
};

int _tmain(int argc, _TCHAR* argv[])
{

	std::vector<Chicken> chicken;
	// ��������� ������ �������� ��������������� ��������
	for(size_t i=0; i<7; ++i)
	{
		chicken.push_back( Chicken::create() );
	}
	
	// ���������� ������ ����� ��������� ������� ������� ����������
	quickSort(chicken.begin(), chicken.end()-1, Chicken::sortByBreeds);

	// ��������� �� ����� ������� � ������� ��� � �������
	std::for_each(chicken.begin(), chicken.end(), Chicken::print);

	return 0;
}

